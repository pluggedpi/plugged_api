<?php 
Class User {
	public $idUser;
	public $name;
	public $birth;
	public $picture;
	public $token;
	
	function __construct($params = array()) {
	        $this->idUser = isset($params['id']) ? $params['id'] : null;
	        $this->name = isset($params['name']) ? $params['name'] : null;
	        $this->birth = isset($params['birth']) ? $params['birth'] : null;
	        $this->picture = isset($params['picture']) ? $params['picture'] : null;
	        $this->token = isset($params['token']) ? $params['token'] : null;
	}

	public function setId($id) {
	        $this->idUser = $id;
	        return $this;
	}

	public function verifyUser() {		
		try {

			$sql = "SELECT * FROM user WHERE idUser = :idUser ";
			$values[':idUser'] = $this->idUser;
		    $result = Db::getRow($sql, $values);
			
			if (is_null($result['idUser'])) {
		    		$return['exists'] = false;
	    		} else {
	    			$return['exists'] = true;
	    			$return['token'] = $result['token'];
	    		}

		} catch (Exception $e) {
			$return['status'] = 500;
	        $return['error'] = $e;	
		}

		return $return;
	}

	public function addUser() {		
		try {

			$sql = "INSERT INTO user(idUser,name,birth,picture,token)
	        VALUES (:idUser,:name,:birth,:picture,:token)";
	        $values = array(
	            ':idUser' => $this->idUser,
	            ':name' => $this->name,
	            ':birth' => $this->birth,
	            ':picture' => $this->picture,
	            ':token' => $this->token
	        );
	        $return = Db::getRow($sql, $values);

	        $sql2 = "SELECT LAST_INSERT_ID();";
	        $result2 = Db::getRow($sql2);
	        $return['idUser'] = $result2['LAST_INSERT_ID()'];

		} catch (Exception $e) {
			$return['status'] = 500;
	        $return['error'] = $e;	
		}

		return $return;
	}

	public function updateUser() {		
		try {

			$sql = "UPDATE user SET name = :name, birth = :birth, picture = :picture, token = :token
	        WHERE idUser = :idUser";
	        $values = array(
	            ':idUser' => $this->idUser,
	            ':name' => $this->name,
	            ':birth' => $this->birth,
	            ':picture' => $this->picture,
	            ':token' => $this->token
	        );
            $return = Db::execute($sql, $values);

		} catch (Exception $e) {
			$return['status'] = 500;
	        $return['error'] = $e;	
		}

		return $return;
	}

	public function getDetails() {		
		try {

			$sqlU = "SELECT name, picture FROM user WHERE idUser = :idUser ";
			$valuesU[':idUser'] = $this->idUser;
	    	$resultU = Db::getRows($sqlU, $valuesU);

			$sql = "SELECT idEvent, title, image, description, address, locationLat, locationLong, dateEvent, idHost, status FROM event WHERE idHost = :idHost ORDER BY idEvent DESC ";
			$values[':idHost'] = $this->idUser;
	    	$result = Db::getRows($sql, $values);

	    	$sql2 = "SELECT event.idEvent, event.title, event.image, event.description, event.address, event.dateEvent, going.idUser, event.idHost, event.status FROM event INNER JOIN going ON event.idEvent = going.idEvent WHERE going.idUser = :idUser AND event.idHost != :idUser AND event.dateEvent >= NOW() ORDER BY event.idEvent DESC ";
			$values2[':idUser'] = $this->idUser;
	    	$result2 = Db::getRows($sql2, $values2);

	    	$sql3 = "SELECT event.idEvent, event.title, event.image, event.description, event.address, event.dateEvent, event.idHost, event.status, category_user.idCategory, going.idUser FROM event JOIN category_event ON event.idEvent = category_event.idEvent JOIN category_user ON category_event.idCategory = category_user.idCategory LEFT JOIN going ON event.idEvent = going.idEvent WHERE category_user.idUser = :idUser AND event.status = 1 AND event.dateEvent >= NOW() AND event.idHost != :idUser AND event.idEvent NOT IN (SELECT idEvent FROM going WHERE going.idUser = :idUser) GROUP BY event.idEvent ORDER BY event.dateEvent";
			$values3[':idUser'] = $this->idUser;
	    	$result3 = Db::getRows($sql3, $values3);

	    	$return['user'] = $resultU;
	    	$return['hosted'] = $result;
	    	$return['going'] = $result2;
	    	$return['recommended'] = $result3;

		} catch (Exception $e) {
			$return['status'] = 500;
	        $return['error'] = $e;	
		}

		return $return;
	}

	public function setGoing($idEvent) {		
		try {

			$sqlSel = "SELECT * FROM going
	        WHERE idUser = :idUser AND idEvent = :idEvent";
	        $valuesSel = array(
	            ':idUser' => $this->idUser,
	            ':idEvent' => $idEvent,
	        );
	        $returnSel = Db::getRows($sqlSel, $valuesSel);
	        $selNum_rows = count($returnSel);

	        if ($selNum_rows == 1) {
	        	$sqlDel = "DELETE FROM going
	       		WHERE idUser = :idUser AND idEvent = :idEvent";
	        	$valuesDel = array(
		            ':idUser' => $this->idUser,
		            ':idEvent' => $idEvent,
		        );
	        	$result = Db::getRow($sqlDel, $valuesDel);
	        	$return['text'] = 'Going'; 
	        } else {
	        	$sql = "INSERT INTO going(idUser,idEvent)
	       		VALUES (:idUser,:idEvent)";
	        	$values = array(
		            ':idUser' => $this->idUser,
		            ':idEvent' => $idEvent,
		        );
	        	$result = Db::getRow($sql, $values);
	        	$return['text'] = 'X Going'; 
	        }

			

		} catch (Exception $e) {
			$return['status'] = 500;
	        $return['error'] = $e;	
		}

		return $return;
	}

	public function setCategories($myCats) {		
		try {

			$sqlDel = "DELETE FROM category_user
	       		WHERE idUser = :idUser ";
        	$valuesDel = array(
	            ':idUser' => $this->idUser
	        );
        	$result = Db::getRow($sqlDel, $valuesDel);

			foreach ($myCats as $cat) {
				$searchSqlCats = "SELECT * FROM category
		        WHERE title LIKE :title ;";
		        $searchValuesCats = array(
					':title' => "%".$cat['name']."%"
		        );
		        $searchResultCats = Db::getRow($searchSqlCats, $searchValuesCats);
		        $idCat = $searchResultCats['idCategory'];

				$sql = "INSERT INTO category_user(idUser,idCategory)
	       		VALUES (:idUser,:idCategory)";
	        	$values = array(
		            ':idUser' => $this->idUser,
		            ':idCategory' => $idCat,
		        );
	        	$result = Db::getRow($sql, $values);
			}			

		} catch (Exception $e) {
			$return['status'] = 500;
	        $return['error'] = $e;	
		}

		return $return;
	}

	public function getCategories() {		
		try {

	    	$sql2 = "SELECT category.title, category.idCategory FROM category INNER JOIN category_user ON category.idCategory = category_user.idCategory WHERE category_user.idUser = :idUser";
			$values2[':idUser'] = $this->idUser;
	    	$result2 = Db::getRows($sql2, $values2);

	    	$return['categories'] = $result2;

		} catch (Exception $e) {
			$return['status'] = 500;
	        $return['error'] = $e;	
		}

		return $return;
	}
}