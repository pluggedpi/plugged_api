<?php
require '../vendor/autoload.php';
require_once '../config/constants.php';
require_once '../config/database.php';
require_once 'classes/Db.php';


$logWriter = new \Slim\LogWriter(fopen('slim_errors.log', 'a'));
$app = new \Slim\Slim(array('log.writer' => $logWriter));
$app->response->headers->set('Content-Type', 'application/json');

//Enable logging
$app->log->setEnabled(true);
$app->log->setLevel(\Slim\Log::ERROR);


function GET_API($app){
	$router = $app->router();
	return $router->getCurrentRoute()->getParams();
}

function POST_API($app){
	return json_decode($app->request()->getBody(), true);
}

function response($data){
	$app = \Slim\Slim::getInstance();
	if (isset($data['status'])) {
		$app->response->setStatus($data['status']);
		unset($data['status']);
	}
	$responseBody = json_encode($data, JSON_NUMERIC_CHECK);
	$app->response->setBody($responseBody);
}


$path = $app->request->getResourceUri();
$path = explode('/', $path);
$path = $path[1];

try {
	Db::setConnectionInfo(_DBNAME_, _USER_, _PASSWORD_, 'mysql', _HOST_);
	require $path.'.php';
} catch (Exception $exception) {
	header('Access-Control-Allow-Origin: *');
	echo $debug ? $exception->getMessage() : 'Ocorreu um erro no servidor.';
}

$app->run();