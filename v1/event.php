<?php 
require_once 'classes/Event.php';

$app->get('/event/details/:idEvent', function ($idEvent) use ($app) {
	$event = new Event;
	$event->setId($idEvent);
	
	$return = $event->getDetails();
	response($return);
});

$app->get('/event/all', function () use ($app) {
	$events = new Event;
	$return = $events->getList();
	response($return);
});

$app->get('/event/:long/:lat/:day', function ($long,$lat,$day) use ($app) {
	$events = new Event;
	$return = $events->getListRegion($long,$lat,$day);
	response($return);
});

$app->post('/event/add', function () use ($app) {

	$params = POST_API($app);
	$event = new Event($params);
	
	$return = $event->addEvent();
	response($return);
});

$app->post('/event/update', function () use ($app) {

	$params = POST_API($app);
	$event = new Event($params);

	$return = $event->updateEvent();
	/*$app->log->error($return);*/
	response($return);
});

$app->get('/event/requirements', function () use ($app) {
	$events = new Event;
	$return = $events->getReqList();
	response($return);
});

$app->get('/event/categories', function () use ($app) {
	$events = new Event;
	$return = $events->getCatList();
	response($return);
});

$app->post('/event/disable', function () use ($app) {

	$params = POST_API($app);
	$event = new Event($params);
	
	$return = $event->disableEvent();
	response($return);
});

$app->post('/event/enable', function () use ($app) {

	$params = POST_API($app);
	$event = new Event($params);
	
	$return = $event->enableEvent();
	response($return);
});