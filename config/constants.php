<?php
// STATUS
define('EVENT_STATUS_DRAFT', 0);
define('EVENT_STATUS_PUBLISHED', 1);
define('EVENT_STATUS_CANCELLED', 2);


// MESSAGES
define('ERROR_DEFAULT', 'Erro inesperado, atualize o navegador e tente novamente!');