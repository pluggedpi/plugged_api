<?php 
Class Event {
	public $idEvent;
	public $idHost;
	public $title;
	public $lat;
	public $long;
	public $address;
	public $description;
	public $dateEvent;
	public $status;
	public $categories;
	public $requirements;
	public $image;

	function __construct($params = array()) {
	        $this->idEvent = isset($params['idEvent']) ? $params['idEvent'] : null;
	        $this->idHost = isset($params['idHost']) ? $params['idHost'] : null;
	        $this->title = isset($params['title']) ? $params['title'] : null;
	        $this->address = isset($params['address']) ? $params['address'] : null;
	        $this->lat = isset($params['locationLat']) ? $params['locationLat'] : null;
	        $this->long = isset($params['locationLong']) ? $params['locationLong'] : null;
	        $this->description = isset($params['description']) ? $params['description'] : null;
	        $this->dateEvent = isset($params['dateEvent']) ? $params['dateEvent'] : null;
	        $this->status = isset($params['status']) ? $params['status'] : null;
	        $this->categories = isset($params['categories']) ? $params['categories'] : null;
	        $this->requirements = isset($params['requirements']) ? $params['requirements'] : null;
	        $this->image = isset($params['image']) ? $params['image'] : null;
	}
	
	public function setId($id) {
	        $this->idEvent = $id;
	        return $this;
	}

	public function getList() {		
		try {

			
			$sql = "SELECT idEvent, title, description, dateEvent, address, locationLat, locationLong, idHost FROM event WHERE status = :status AND dateEvent >= NOW() ";			
			$values[':status'] = EVENT_STATUS_PUBLISHED;
	    	$result = Db::getRows($sql, $values);
	    	$return = $result;
	    	
	    	foreach ($result as $key => $res) {
	    		$return[$key]['idHost'] = $res['idHost'].'a';
	    	}

	    	

		} catch (Exception $e) {
			$return['status'] = 500;
	        $return['error'] = $e;	
		}

		return $return;
	}

	public function getListRegion($long,$lat,$day) {		
		try {

			
			$sql = "SELECT idEvent, title, description, dateEvent, address, locationLat, locationLong, idHost, status, ROUND(( 6371 * acos( cos( radians(:lat) ) * cos( radians( locationLat ) ) * cos( radians( locationLong ) - radians(:long) ) + sin( radians(:lat) ) * sin( radians( locationLat ) ) ) ),(2) ) AS distance FROM event HAVING distance < 100 AND status = :status AND dateEvent >= NOW() AND DATE(dateEvent) = :day";			
			$values[':status'] = EVENT_STATUS_PUBLISHED;
			$values[':lat'] = $lat;
			$values[':long'] = $long;
			$values[':day'] = $day;
	    	$result = Db::getRows($sql, $values);
	    	$return = $result;
	    	
	    	foreach ($result as $key => $res) {
	    		$return[$key]['idHost'] = $res['idHost'].'a';
	    	}

	    	

		} catch (Exception $e) {
			$return['status'] = 500;
	        $return['error'] = $e;	
		}

		return $return;
	}
	
	public function getDetails() {		
		try {
			$sql = "SELECT * FROM user INNER JOIN event ON user.idUser = event.idHost WHERE event.idEvent = :id";
			//$sql = "SELECT * FROM event WHERE idEvent = :id";
			$values[':id'] = $this->idEvent;
		    $result = Db::getRows($sql, $values);

		    $sqlCats = "SELECT title FROM `category` INNER JOIN `category_event` ON category.idCategory = category_event.idCategory WHERE category_event.idEvent = :idEvent";
		    $valuesCats[':idEvent'] = $this->idEvent;
		    $resultCats = Db::getRows($sqlCats, $valuesCats);
		    foreach ($resultCats as $cat) {
		    	$catswithname[]['name'] = $cat['title']; 
		    }

		    $sqlReqs = "SELECT title FROM `requirement` INNER JOIN `requirement_event` ON requirement.idRequirement = requirement_event.idRequirement WHERE requirement_event.idEvent = :idEvent";
		    $valuesReqs[':idEvent'] = $this->idEvent;
		    $resultReqs = Db::getRows($sqlReqs, $valuesReqs);
		    foreach ($resultReqs as $req) {
		    	$reqswithname[]['name'] = $req['title']; 
		    }

		    $sqlGoing = "SELECT user.idUser, user.name, user.picture FROM `user` INNER JOIN `going` ON user.idUser = going.idUser WHERE going.idEvent = :idEvent";
		    $valuesGoing[':idEvent'] = $this->idEvent;
		    $resultGoing = Db::getRows($sqlGoing, $valuesGoing);

		    $resultGoinglength = count($resultGoing);
		    for ($i=0; $i < $resultGoinglength; $i++) { 
		    	$resultGoing[$i]['idUser'] = $resultGoing[$i]['idUser'].'a';
		    }

		    $return['infos'] = $result;
		    $return['infos'][0]['idHost'] = $return['infos'][0]['idHost'].'a';
		    $return['infos'][0]['idUser'] = $return['infos'][0]['idUser'].'a';
		    $return['cats'] = $catswithname;
		    $return['reqs'] = $reqswithname;
		    $return['going'] = $resultGoing;

		} catch (Exception $e) {
			$return['status'] = 500;
	        $return['error'] = $e;	
		}

		return $return;
	}

	public function addEvent() {		
		try {

			define('UPLOAD_DIR', '/home/anade380/public_html/plugged/api/v1/uploads/');
			$img = $this->image;
			$data = base64_decode($img);
			$file = UPLOAD_DIR . $this->idHost . '-' . time() . '.png';
			//$success = file_put_contents($file, $data);
			$success = imagejpeg(imagecreatefromstring($data),$file,80);

			if ($success) {
				$url = str_replace("/home/anade380/public_html/plugged/api/v1/uploads/", "http://$_SERVER[HTTP_HOST]/plugged/api/v1/uploads/", $file);
				$return['image'] = $url;
			} else {
				$return['image'] = 'Unable to save the file.';
			}

			$sql = "INSERT INTO event(idHost,title,address,locationLat,locationLong,dateEvent,description,status,image)
		        VALUES (:idHost,:title,:address,:lat,:long,:dateEvent,:description,:status,:image);";
	        $values = array(
				':idHost' => $this->idHost,
				':title' => $this->title,
				':address' => $this->address,
				':lat' => $this->lat,
				':long' => $this->long,
				':description' => $this->description,
				':dateEvent' => $this->dateEvent,
				':status' => $this->status,
				':image' => $url
	        );
		    $result = Db::getRow($sql, $values); 

		    $sql2 = "SELECT LAST_INSERT_ID();";
		        $result2 = Db::getRow($sql2);
		        $return['idEvent'] = $result2['LAST_INSERT_ID()'];

		    $sqlGoing = "INSERT INTO going(idUser,idEvent)
		        VALUES (:idHost,:idEvent);";
		        $valuesGoing = array(
					':idHost' => $this->idHost,
					':idEvent' => $return['idEvent']
		        );
		        $resultGoing = Db::getRow($sqlGoing, $valuesGoing);

		    if ($this->categories !== null && $this->categories !== '') {
		    
			    foreach ($this->categories as $cat) {
			    	$return['catname'] = $cat['name'];
			        $searchSqlCats = "SELECT * FROM category
				        WHERE title LIKE :title ;";
				        $searchValuesCats = array(
							':title' => "%".$cat['name']."%"
				        );
				        $searchResultCats = Db::getRow($searchSqlCats, $searchValuesCats);
				        $return['searchResultCats'] = $searchResultCats['idCategory'];

				      	if ($searchResultCats) {
				        	$eventCategories = $searchResultCats['idCategory'];
				        } else {
				        	$sqlNewCat = "INSERT INTO category(title)
					        VALUES (:title);";
					        $valuesNewCat = array(
								':title' => $cat['name']
					        );
					        $resultNewCat = Db::getRow($sqlNewCat, $valuesNewCat);
					        
					        $sqlNC2 = "SELECT LAST_INSERT_ID();";
					        $resultNC2 = Db::getRow($sqlNC2);
					        $eventCategories = $resultNC2['LAST_INSERT_ID()'];
				        }

				        $sqlCats = "INSERT INTO category_event(idEvent,idCategory)
				        VALUES (:idEvent,:idCategory);";
				        $valuesCats = array(
							':idEvent' => $return['idEvent'],
							':idCategory' => $eventCategories
				        );
				        $resultCats = Db::getRow($sqlCats, $valuesCats);
				        	        
				}
			}

			if ($this->requirements !== null && $this->requirements !== '') {

			    foreach ($this->requirements as $req) {
			    	
		        	$searchSqlReqs = "SELECT * FROM requirement
			        WHERE title LIKE :title ;";
			        $searchValuesReqs = array(
						':title' => "%".$req['name']."%"
			        );
			        $searchResultReqs = Db::getRow($searchSqlReqs, $searchValuesReqs);
			        
			        if ($searchResultReqs) {
			        	$eventRequirements = $searchResultReqs['idRequirement'];
			        } else {
			        	$sqlNewReq = "INSERT INTO requirement(title)
				        VALUES (:title);";
				        $valuesNewReq = array(
							':title' => $req['name']
				        );
				        $resultNewReq = Db::getRow($sqlNewReq, $valuesNewReq);
				        
				        $sqlNR2 = "SELECT LAST_INSERT_ID();";
				        $resultNR2 = Db::getRow($sqlNR2);
				        $eventRequirements = $resultNR2['LAST_INSERT_ID()'];
			        }


			        $sqlReqs = "INSERT INTO requirement_event(idEvent,idRequirement)
			        VALUES (:idEvent,:idRequirement);";
			        $valuesReqs = array(
						':idEvent' => $return['idEvent'],
						':idRequirement' => $eventRequirements
			        );
			        $resultReqs = Db::getRow($sqlReqs, $valuesReqs);
			     
			   
			    }
			}
		} catch (Exception $e) {
			$return['status'] = 500;
	        $return['error'] = $e;	
		}

		return $return;
	}

public function updateEvent() {		
		try {

			$img = $this->image;
			if (strpos($img, 'http://') !== false || strpos($img, 'https://') !== false) {
			    $url = $img;
				$return['image'] = $url;
			} else {
				define('UPLOAD_DIR', '/home/anade380/public_html/plugged/api/v1/uploads/');
				$data = base64_decode($img);
				$file = UPLOAD_DIR . $this->idHost . '-' . time() . '.png';
				$success = imagejpeg(imagecreatefromstring($data),$file,80);

				if ($success) {
					$url = str_replace("/home/anade380/public_html/plugged/api/v1/uploads/", "http://$_SERVER[HTTP_HOST]/plugged/api/v1/uploads/", $file);
					$return['image'] = $url;
				} else {
					$return['image'] = 'Unable to save the file.';
				}
			}
			

			$sql = "UPDATE event SET title = :title, address = :address, locationLat = :lat, locationLong = :long, dateEvent = :dateEvent, description = :description, image = :image WHERE idEvent = :idEvent;";
	        $values = array(
				':idEvent' => $this->idEvent,
				':title' => $this->title,
				':address' => $this->address,
				':lat' => $this->lat,
				':long' => $this->long,
				':description' => $this->description,
				':dateEvent' => $this->dateEvent,
				':image' => $url
	        );
		    $result = Db::getRow($sql, $values); 

		    $sqlremovecats = "DELETE FROM category_event WHERE idEvent = :idEvent;";
		    $valuesremovecats = array(':idEvent' => $this->idEvent);
		    $resultremovecats = Db::getRow($sqlremovecats, $valuesremovecats); 

		    if ($this->categories !== null && $this->categories !== '') {
		    
			    foreach ($this->categories as $cat) {
			    	
			        $searchSqlCats = "SELECT * FROM category
				        WHERE title LIKE :title ;";
				        $searchValuesCats = array(
							':title' => "%".$cat['name']."%"
				        );
				        $searchResultCats = Db::getRow($searchSqlCats, $searchValuesCats);
				        

				      	if ($searchResultCats['idCategory']) {
				        	$eventCategories = $searchResultCats['idCategory'];
				        } else {
				        	$sqlNewCat = "INSERT INTO category(title)
					        VALUES (:title);";
					        $valuesNewCat = array(
								':title' => $cat['name']
					        );
					        $resultNewCat = Db::getRow($sqlNewCat, $valuesNewCat);
					        
					        $sqlNC2 = "SELECT LAST_INSERT_ID();";
					        $resultNC2 = Db::getRow($sqlNC2);
					        $eventCategories = $resultNC2['LAST_INSERT_ID()'];
				        }

				        $sqlCats = "INSERT INTO category_event(idEvent,idCategory)
				        VALUES (:idEvent,:idCategory);";
				        $valuesCats = array(
							':idEvent' => $this->idEvent,
							':idCategory' => $eventCategories
				        );
				        $resultCats = Db::getRow($sqlCats, $valuesCats);
				        	        
				}
			}

			$sqlremovereqs = "DELETE FROM requirement_event WHERE idEvent = :idEvent;";
		    $valuesremovereqs = array(':idEvent' => $this->idEvent);
		    $resultremovereqs = Db::getRow($sqlremovereqs, $valuesremovereqs); 

			if ($this->requirements !== null && $this->requirements !== '') {

			    foreach ($this->requirements as $req) {
			    	
		        	$searchSqlReqs = "SELECT * FROM requirement
			        WHERE title LIKE :title ;";
			        $searchValuesReqs = array(
						':title' => "%".$req['name']."%"
			        );
			        $searchResultReqs = Db::getRow($searchSqlReqs, $searchValuesReqs);
			        
			        if ($searchResultReqs) {
			        	$eventRequirements = $searchResultReqs['idRequirement'];
			        } else {
			        	$sqlNewReq = "INSERT INTO requirement(title)
				        VALUES (:title);";
				        $valuesNewReq = array(
							':title' => $req['name']
				        );
				        $resultNewReq = Db::getRow($sqlNewReq, $valuesNewReq);
				        
				        $sqlNR2 = "SELECT LAST_INSERT_ID();";
				        $resultNR2 = Db::getRow($sqlNR2);
				        $eventRequirements = $resultNR2['LAST_INSERT_ID()'];
			        }


			        $sqlReqs = "INSERT INTO requirement_event(idEvent,idRequirement)
			        VALUES (:idEvent,:idRequirement);";
			        $valuesReqs = array(
						':idEvent' => $this->idEvent,
						':idRequirement' => $eventRequirements
			        );
			        $resultReqs = Db::getRow($sqlReqs, $valuesReqs);
			     
			   
			    }
			}
		} catch (Exception $e) {
			$return['status'] = 500;
	        $return['error'] = $e;	
		}

		return $return;
	}

	public function getReqList() {		
		try {

			$sql = "SELECT * FROM requirement";
	    	$result = Db::getRows($sql);

	    	$return = $result;

		} catch (Exception $e) {
			$return['status'] = 500;
	        $return['error'] = $e;	
		}

		return $return;
	}

	public function getCatList() {		
		try {

			$sql = "SELECT * FROM category";
	    	$result = Db::getRows($sql);

	    	$return = $result;

		} catch (Exception $e) {
			$return['status'] = 500;
	        $return['error'] = $e;	
		}

		return $return;
	}

	public function disableEvent() {		
		try {

			$sql = "UPDATE event  
	        SET status = 0 WHERE idEvent = :idEvent;";
	        $values = array(
				':idEvent' => $this->idEvent
	        );
	        $result = Db::getRow($sql, $values);
	        var_dump($result);	        

		} catch (Exception $e) {
			$return['status'] = 500;
	        $return['error'] = $e;	
		}

		return $return;
	}

	public function enableEvent() {		
		try {

			$sql = "UPDATE event  
	        SET status = 1 WHERE idEvent = :idEvent;";
	        $values = array(
				':idEvent' => $this->idEvent
	        );
	        $result = Db::getRow($sql, $values);
	        var_dump($result);	        

		} catch (Exception $e) {
			$return['status'] = 500;
	        $return['error'] = $e;	
		}

		return $return;
	}

}