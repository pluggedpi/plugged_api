<?php
require_once 'classes/User.php';

$app->get('/user/:id', function () use ($app) {
	
});

$app->post('/user/login', function () use ($app) {

	$params = POST_API($app);
	
	//consulta se existe
	$user = new User($params);
	$veru = $user->verifyUser();

	if (!$veru['exists']) {
		if ($user->addUser()) {
			$return['user_status'] = 'Created';
		}
	} else {
		if ($update = $user->updateUser()) {
			$return['user_status'] = 'Updated';
		}
	}	

	response($return);
});

$app->get('/user/:idUser/events', function ($idUser=null) use ($app) {
	$user = new User;
	$user->setId($idUser);
	
	$return = $user->getDetails();
	response($return);
});

$app->post('/user/going', function () use ($app) {

	$params = POST_API($app);
	
	$user = new User($params);

	$return = $user->setGoing($params['idEvent']);
	response($return);
});

$app->post('/user/categories', function () use ($app) {

	$params = POST_API($app);
	
	$user = new User($params);

	$return = $user->setCategories($params['myCats']);
	response($return);
});

$app->get('/user/:idUser/categories', function ($idUser=null) use ($app) {
	$user = new User;
	$user->setId($idUser);
	
	$return = $user->getCategories();
	response($return);
});